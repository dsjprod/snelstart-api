<?php

namespace DSJ\SnelstartApiBundle\Repository;

use DSJ\SnelstartApiBundle\Traits\Get;
use DSJ\SnelstartApiBundle\Traits\Update;

class RelatieCustomFields
{
    use Get;
    use Update;

    public function __construct($oSnelStartManager)
    {
        $this->oSnelStartManager = $oSnelStartManager;
        $this->endPoint          = $this->oSnelStartManager->setRepositoryEndpoint(__FILE__);
    }
}
