<?php

namespace DSJ\SnelstartApiBundle\Repository;

use DSJ\SnelstartApiBundle\Traits\Get;
use DSJ\SnelstartApiBundle\Traits\Delete;
use DSJ\SnelstartApiBundle\Traits\Update;
use DSJ\SnelstartApiBundle\Traits\Create;
use DSJ\SnelstartApiBundle\Traits\Bijlagen;

class InkoopBoekingen
{
    use Get;
    use Delete;
    use Update;
    use Create;
    use Bijlagen;

    public function __construct($oSnelStartManager)
    {
        $this->oSnelStartManager = $oSnelStartManager;
        $this->endPoint          = $this->oSnelStartManager->setRepositoryEndpoint(__FILE__);
    }

    public function createUbl($aData)
    {
        return $this->oSnelStartManager->request('POST', $this->endPoint . '/ubl', [
            'form_params' => $aData,
        ]);
    }
}
