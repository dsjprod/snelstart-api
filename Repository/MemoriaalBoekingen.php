<?php

namespace DSJ\SnelstartApiBundle\Repository;

use DSJ\SnelstartApiBundle\Traits\Get;
use DSJ\SnelstartApiBundle\Traits\Delete;
use DSJ\SnelstartApiBundle\Traits\Update;
use DSJ\SnelstartApiBundle\Traits\Create;

class MemoriaalBoekingen
{
    use Get;
    use Delete;
    use Update;
    use Create;

    public function __construct($oSnelStartManager)
    {
        $this->oSnelStartManager = $oSnelStartManager;
        $this->endPoint          = $this->oSnelStartManager->setRepositoryEndpoint(__FILE__);
    }
}
