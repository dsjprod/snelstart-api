<?php

namespace DSJ\SnelstartApiBundle\Repository;

use DSJ\SnelstartApiBundle\Traits\Get;
use DSJ\SnelstartApiBundle\Traits\Delete;
use DSJ\SnelstartApiBundle\Traits\Update;
use DSJ\SnelstartApiBundle\Traits\Create;
use DSJ\SnelstartApiBundle\Traits\Bijlagen;

class VerkoopBoekingen
{
    use Get;
    use Delete;
    use Update;
    use Create;
    use Bijlagen;

    public function __construct($oSnelStartManager)
    {
        $this->oSnelStartManager = $oSnelStartManager;
        $this->endPoint          = $this->oSnelStartManager->setRepositoryEndpoint(__FILE__);
    }
}
