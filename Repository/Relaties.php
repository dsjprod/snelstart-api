<?php

namespace DSJ\SnelstartApiBundle\Repository;

use DSJ\SnelstartApiBundle\Traits\GetAll;
use DSJ\SnelstartApiBundle\Traits\Get;
use DSJ\SnelstartApiBundle\Traits\Delete;
use DSJ\SnelstartApiBundle\Traits\Update;
use DSJ\SnelstartApiBundle\Traits\Create;

class Relaties
{
    use GetAll;
    use Get;
    use Delete;
    use Update;
    use Create;

    public function __construct($oSnelStartManager)
    {
        $this->oSnelStartManager = $oSnelStartManager;
        $this->endPoint          = $this->oSnelStartManager->setRepositoryEndpoint(__FILE__);
    }

    public function getIncassoMachtigingen($id)
    {
        return $this->oSnelStartManager->request('GET',  $this->endPoint . '/' . $id . '/doorlopendeincassomachtigingen');
    }

    public function getInkoopBoekingen($id)
    {
        return $this->oSnelStartManager->request('GET',  $this->endPoint . '/' . $id . '/inkoopboekingen');
    }

    public function getVerkoopBoekingen($id)
    {
        return $this->oSnelStartManager->request('GET',  $this->endPoint . '/' . $id . '/verkoopboekingen');
    }
}
