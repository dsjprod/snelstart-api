<?php

namespace DSJ\SnelstartApiBundle\Repository;

use DSJ\SnelstartApiBundle\Traits\GetAll;
use DSJ\SnelstartApiBundle\Traits\Get;

class Landen
{
    use GetAll;
    use Get;

    public function __construct($oSnelStartManager)
    {
        $this->oSnelStartManager = $oSnelStartManager;
        $this->endPoint          = $this->oSnelStartManager->setRepositoryEndpoint(__FILE__);
    }
}
