**[Requirements](#markdown-header-requirements)** |
**[Installation](#markdown-header-installation)** |
**[Usage](#markdown-header-usage)** |
**[Features](#markdown-header-features)** |
**[Suggestions](#markdown-header-suggestions)** |
**[Contributing](#markdown-header-contributing)** |
**[License](#markdown-header-license)**

# SnelstartApiBundle

[![Total Downloads](https://poser.pugx.org/dsj/snelstartapi/downloads.png)](https://packagist.org/packages/dsj/snelstartapi)
[![Monthly Downloads](https://poser.pugx.org/dsj/snelstartapi/d/monthly.png)](https://packagist.org/packages/dsj/snelstartapi)
[![Latest Stable Version](https://poser.pugx.org/dsj/snelstartapi/v/stable.png)](https://packagist.org/packages/dsj/snelstartapi)
[![License](https://poser.pugx.org/dsj/snelstartapi/license)](https://packagist.org/packages/dsj/snelstartapi)


This bundle integrates the [Snelstart Api][1] into Symfony. Snelstart is a bookkeeping cloud software with the ability to integrate any existing CRM software seamlessly.

[![](http://i.imgur.com/tHWKVSL.png)]()
This package was created with love by [DSJ Productions][5]

SnelstartApiBundle follows semantic versioning. Read more on [semver.org][2].

----

## Requirements
 - PHP 5.3 or above
 - Symfony 2.5 or above
 - [Guzzle][6] (included by composer)
 - [Symfony Cache Component][3] (included by composer)

----

## Installation
To install this bundle, run the command below and you will get the latest version by [Packagist][4].

``` bash
composer require dsj/snelstartapi
```

To use the newest (maybe unstable) version please add following into your composer.json (not recommended, api might change):

``` json
{
    "require": {
        "dsj/snelstartapi": "dev-master"
    }
}
```

----

## Usage
Load bundle in AppKernel.php:
``` php
new DSJ\SnelstartApiBundle\SnelstartApiBundle()
```

Configuration in parameters.yml:
``` yaml
parameters:
    snelstart_application_key: the application key of your snelstart account link
    snelstart_primary_key:     the primary key of your snelstart account
    snelstart_secondary_key:   the secondary key of your snelstart account
    ...
```
All of these settings are **REQUIRED**.

Using services in controller:
``` php
/** @var \DSJ\SnelstartApiBundle\Manager\SnelStartManager $oSnelStartManager */
$oSnelStartManager = $this->get('dsj.snelstart');

// Do a raw GET request
$aCustomers = $oSnelStartManager->request('GET', 'relaties');

// Or call via this easy to remember syntax
$aCustomers = $oSnelStartManager->Relaties->getAll();

// Do a raw POST request
$aCustomers = $oSnelStartManager->request('POST', 'relaties', [
    'naam' => 'test',
]);

// Or call via this easy to remember syntax
$aCustomers = $oSnelStartManager->Relaties->create([
    'naam' => 'test',
]);
```

----

## Features

 - Missing parameters displayed on faulty requests thanks to swagger file integeration
 - Easy callable endpoints (`$oSnelStartManager->Relaties->getAll()`, `$oSnelStartManager->Relaties->get($id)`)
 - Token cache with invalidation
 - Reqeust retries on failed responses
 - Authentication happens automagically, you just supply the needed keys in configuration

----

## Good to know

 - Calling endpoints which returns a `null` means the object does not exist or the `array` is empty
 - Your endpoints will return a `stdClass` object or `array` depending on the type of endpoint you have chosen.

----

## Contributing
👍 If you would like to contribute to the project, please read the [CONTRIBUTING.md](CONTRIBUTING.md).

🎉 Thanks to the contributors who participated in this project.

----

## License
This bundle is released under the [MIT license](LICENSE)


[1]: https://www.snelstart.nl/
[2]: http://semver.org/
[3]: https://packagist.org/packages/symfony/cache
[4]: https://packagist.org/packages/dsj/snelstartapi
[5]: https://dsj.nl
[6]: https://packagist.org/packages/guzzlehttp/guzzle
