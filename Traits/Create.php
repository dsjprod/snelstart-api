<?php

namespace DSJ\SnelstartApiBundle\Traits;

trait Create
{
    public function create($aData)
    {
        return $this->oSnelStartManager->request('POST', $this->endPoint, [
            'form_params' => $aData,
        ]);
    }
}
