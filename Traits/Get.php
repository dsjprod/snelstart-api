<?php

namespace DSJ\SnelstartApiBundle\Traits;

trait Get
{
    public function get($id)
    {
        return $this->oSnelStartManager->request('GET',  $this->endPoint . '/' . $id);
    }
}
