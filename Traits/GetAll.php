<?php

namespace DSJ\SnelstartApiBundle\Traits;

trait GetAll
{
    public function getAll()
    {
        return $this->oSnelStartManager->request('GET',  $this->endPoint);
    }
}
