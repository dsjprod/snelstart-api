<?php

namespace DSJ\SnelstartApiBundle\Traits;

trait Delete
{
    public function delete($id)
    {
        return $this->oSnelStartManager->request('DELETE', $this->endPoint .  '/' . $id);
    }
}
