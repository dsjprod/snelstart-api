<?php

namespace DSJ\SnelstartApiBundle\Traits;

trait Update
{
    public function update($id, $aData)
    {
        return $this->oSnelStartManager->request('PUT', $this->endPoint .  '/' . $id, [
            'form_params' => $aData,
        ]);
    }
}
