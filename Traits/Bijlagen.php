<?php

namespace DSJ\SnelstartApiBundle\Traits;

trait Bijlagen
{
    public function getAllBijlagen($id)
    {
        return $this->oSnelStartManager->request('GET', $this->endPoint . '/' . $id . '/bijlagen');
    }

    public function createBijlagen($id, $aData)
    {
        return $this->oSnelStartManager->request('POST', $this->endPoint . '/' . $id . '/bijlagen', [
            'form_params' => $aData,
        ]);
    }

    public function deleteBijlagen($id, $bid)
    {
        return $this->oSnelStartManager->request('POST', $this->endPoint . '/' . $id . '/bijlagen/' . $bid);
    }

    public function getBijlagen($id, $bid)
    {
        return $this->oSnelStartManager->request('GET', $this->endPoint . '/' . $id . '/bijlagen/' . $bid);
    }

    public function updateBijlagen($id, $bid, $aData)
    {
        return $this->oSnelStartManager->request('GET', $this->endPoint . '/' . $id . '/bijlagen/' . $bid, [
            'form_params' => $aData,
        ]);
    }
}
