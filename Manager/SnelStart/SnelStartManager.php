<?php

namespace DSJ\SnelstartApiBundle\Manager\SnelStart;

ini_set('xdebug.var_display_max_depth', 15);
ini_set('xdebug.var_display_max_children', 256);
ini_set('xdebug.var_display_max_data', 1024);

use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\Finder\Finder;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Query;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;

// TODO
// Type validate input in the function verifyRoute?
// bijlagen traits (endpoints) (BID)

class SnelStartManager
{
    private $aSettings = [
        'creds' => [
            'application' => '',
            'primary'     => '',
            'secondary'   => '',
        ],
        'api' => [
            'auth' => [
                'url'  => 'https://auth.snelstart.nl/b2b/',
            ],
            'base'  => [
                'url' => 'https://b2bapi.snelstart.nl/v1/',
            ],
        ],
        'allowedtypes' => [
            'GET',
            'PUT',
            'DELETE',
            'PATCH',
            'POST',
        ],
        'timeouts' => [
            'request'    => 30,
            'connection' => 30,
        ],
    ];

    public function __construct($oKernel, $sApplicationKey, $sPrimaryKey, $sSecondaryKey)
    {
        $this->oGuzzle = new \GuzzleHttp\Client([
            'connect_timeout' => $this->aSettings['timeouts']['connection'],
            'timeout'         => $this->aSettings['timeouts']['request'],
        ]);

        $this->oCache   = new FilesystemAdapter();
        $this->iRetries = 0;
        $this->oKernel  = $oKernel;

        $this->aSettings['creds']['application'] = $sApplicationKey;
        $this->aSettings['creds']['primary']     = $sPrimaryKey;
        $this->aSettings['creds']['secondary']   = $sSecondaryKey;

        $this->loadSwaggerJSON();

        // FetchDefinitions needs to be called AFTER fetchEndpoints
        $this->fetchEndpoints();
        $this->fetchDefinitions();
        $this->setRepositories();

        $this->generateAccessToken();
    }

    // Load the repository classes
    private function setRepositories()
    {
        $oFinder = new finder();
        $oFinder->files()->name('*.php')->in($this->oKernel->locateResource('@SnelstartApiBundle' . DIRECTORY_SEPARATOR . 'Repository'));

        foreach ($oFinder as $oFile) {
            if ($oFile->getType() !== 'file') {
                continue;
            }

            $sClassName        = pathinfo(basename($oFile->getFilename()), PATHINFO_FILENAME);
            $sFullClassName    = 'DSJ\\SnelstartApiBundle\\Repository\\' . $sClassName;
            $this->$sClassName = new $sFullClassName($this);
        }
    }

    public function setRepositoryEndpoint($sFile)
    {
        return strtolower(pathinfo(basename($sFile), PATHINFO_FILENAME));
    }

    // Load the SnelStart api via the generated swagger file (woo0000t)
    private function loadSwaggerJSON()
    {
        $sSwaggerFile = $this->oKernel->locateResource('@SnelstartApiBundle/Resources/config/swagger.json');
        $this->aSwagger = json_decode(file_get_contents($sSwaggerFile));

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new \Exception('Could not parse the swagger.json file, Is it valid JSON?');
        }
    }

    // Matches a model reference with a definition
    private function findReferenceDefinition($sWillMatchReference)
    {
        $oMatchedReference = null;

        foreach ($this->aSwagger->definitions as $sDefinition => $oDefinition) {
            if ($sDefinition === $sWillMatchReference) {
                $oMatchedReference = $oDefinition;
            }
        }

        return $oMatchedReference;
    }

    // Finds out if a type is optional from text, which is mostly the description field
    private function isOptional($oProp)
    {
        if (property_exists($oProp, '$description')) {
            if (strpos($oProp->{'description'}, '(optioneel)') !== false) {
                return true;
            }
        }

        return false;
    }

    // Binds a model representation from the model name and prop name
    private function fetchTypeFromModel($sModel, $sPropName, $oRouteMetadata)
    {
        $aReturn    = [];
        $oReference = $this->findReferenceDefinition($sModel);

        foreach ($oReference->properties as $sSubPropName => $oProp) {
            $aReturn[$sSubPropName] = $this->defineTypes($oProp, $sSubPropName, $oRouteMetadata);
        }

        return $aReturn;
    }

    // What is a type? it's a field format of a record
    // We want to know the requirements and understand the format
    private function defineTypes($oProp, $sPropName, $oRouteMetadata)
    {
        $aRouteProp = [];

        if (property_exists($oProp, 'type')) {
            $aRouteProp['type']     = $oProp->type;

            $aRouteProp['optional'] = $this->isOptional($oProp);
            $oItemSelector          = null;

            if (property_exists($oProp, 'items')) {
                $oItemSelector = $oProp->items;
            } elseif (property_exists($oProp, 'enum')) {
                $oItemSelector = $oProp->enum;
            }

            // Nested!
            if ($oItemSelector !== null) {
                if (gettype($oItemSelector) === 'object') {
                    if (property_exists($oItemSelector, 'enum')) {
                        $aRouteProp['items'] = [];
                        $aRouteProp['type']  = 'array';
                        foreach ($oItemSelector->enum as $key => $value) {
                            $aRouteProp['items'][$value] = [
                                'type' => $oItemSelector->type,
                            ];
                        }
                    } elseif (property_exists($oItemSelector, '$ref')) {
                        $sModel                 = explode('#/definitions/', $oProp->items->{'$ref'})[1];
                        $aRouteProp['items']    = $this->fetchTypeFromModel($sModel, $sPropName, $oRouteMetadata);
                        $aRouteProp['type']     = 'array';
                        $aRouteProp['relation'] = $sModel;
                    } else {
                        throw new \Exception('oProp type was a array but no enum or $ref could be found');
                    }
                } else {
                    $aRouteProp['items'] = [];
                    $aRouteProp['type']  = 'array';
                    foreach ($oItemSelector as $key => $value) {
                        $aRouteProp['items'][$value] = [
                            'type' => 'string', // TODO: type should be dynamic
                        ];
                    }
                }
            }
        }

        // If there is no type field present, could be just a reference
        if (property_exists($oProp, 'type') === false) {
            if (property_exists($oProp, '$ref')) {
                $sModel                 = explode('#/definitions/', $oProp->{'$ref'})[1];
                $aRouteProp['relation'] = $sModel;
                $aRouteProp['type']     = 'array';
                $aRouteProp['items']    = $this->fetchTypeFromModel($sModel, $sPropName, $oRouteMetadata);
            } else {
                throw new \Exception('Unhandled exception');
            }
        }

        return $aRouteProp;
    }

    // Fill definition data in the $this->aEndpoints variable
    // Definitions are references to models and endpoints
    private function fetchDefinitions()
    {
        // Make sure to keep references here
        foreach ($this->aEndpoints as $sRoute => &$oRoute) {
            foreach ($oRoute as $sMethod => &$oRouteMetadata) {
                if ($oRouteMetadata['relation'] === false) {
                    continue;
                }

                $oMatchedReference = $this->findReferenceDefinition($oRouteMetadata['relation']['reference']);

                $oRouteMetadata['definitions'] = [];
                if ($oMatchedReference) {
                    foreach ($oMatchedReference->properties as $sPropName => $oProp) {
                        $oRouteMetadata['definitions'][$sPropName] = [];

                        // Defines all types (recursivly)
                        $oRouteMetadata['definitions'][$sPropName] = $this->defineTypes($oProp, $sPropName, $oRouteMetadata);
                    }
                }
            }
        }
    }

    // Gets a model and reference from a set of text, which is mostly in the description.
    private function getRelation($sText)
    {
        // Haystack example:
        // Format - uuid. De identifier als {System.Guid} uit het {SnelStart.B2B.Api.V1.Models.Relaties.RelatieModel} van de te wijziging relatie.

        // Match example:
        // Relaties.RelatieModel
        $iMatched = preg_match_all('/Models.\s*([^}]*)/', $sText, $aMatches);

        if (boolval($iMatched)) {
            if (isset($aMatches[1][0]) === false) {
                throw new \Exception('Could not determine relation model from the text: ' . $sText);
            }

            $aModel = explode('.', $aMatches[1][0]);

            if (isset($aModel[0]) === false || isset($aModel[1]) === false) {
                throw new \Exception('Model or reference is missing for the text: ' . $sText);
            }

            return [
                'model'     => $aModel[0],
                'reference' => $aModel[1],
            ];
        }

        return false;
    }

    // Fill endpoint data from the SnelStart api swagger file
    private function fetchEndpoints()
    {
        $oPaths                                = $this->aSwagger->paths;
        $aSettings['api']['base']['endpoints'] = [];
        $aRoutes                               = [];

        // For this route
        foreach ($oPaths as $sRoute => $oRoute) {
            $aRoutes[$sRoute] = [];
            // For this route method
            foreach ($oRoute as $sMethod => $oRouteMetadata) {
                // Define the method
                $sMethod = strtoupper($sMethod);
                $aRoutes[$sRoute][$sMethod] = [];

                // Define parameters
                $aRoutes[$sRoute][$sMethod]['parameters'] = [];

                // Lets define a short hand for easy reading
                $aParams = &$aRoutes[$sRoute][$sMethod]['parameters'];

                foreach ($oRouteMetadata->parameters as $i => $oParameter) {
                    // If this parameter needs to be defined in the querystring
                    if ($oParameter->in === 'path') {
                        $aParams[$oParameter->name] = [
                            'type' => $oParameter->type,
                        ];

                        $bIsRequired = false;
                        if (property_exists($oParameter, 'required')) {
                            $bIsRequired = $oParameter->required;
                        }

                        $aParams[$oParameter->name]['required'] = $bIsRequired;
                    }
                }

                // Define the relation model from this endpoint
                $sRelationModel = $this->getRelation($oRouteMetadata->description);
                if ($sRelationModel === false) {
                    foreach ($oRouteMetadata->parameters as $i => $oParameter) {
                        if (property_exists($oParameter, 'description')) {
                            $sRelationModel = $this->getRelation($oParameter->description);
                        }
                    }
                }
                $aRoutes[$sRoute][$sMethod]['relation'] = $sRelationModel;
            }
        }

        $this->aEndpoints = $aRoutes;
    }

    // Matches a method and url with a api endpoint
    private function matchEndpoint($sType, $sUrl)
    {
        foreach ($this->aEndpoints as $sRoute => $oRoute) {
            // Define allowed methods for this route
            $aAllowedMethods = [];
            $sAllowedMethods = '';
            foreach ($oRoute as $sMethod => $oRouteMetadata) {
                $aAllowedMethods[] = $sMethod;
                $sAllowedMethods .= $sMethod . ' ';
            }

            // First find the parameters from the url
            // Parameters are expected to be UUID's

            $aFoundParams = [];
            foreach (explode('/', $sUrl) as $i => $sUrlPart) {
                $iMatched = preg_match_all('/[A-F0-9]{8}-[A-F0-9]{4}-4[A-F0-9]{3}-[89AB][A-F0-9]{3}-[A-F0-9]{12}/i', $sUrlPart, $aMatches);

                if (boolval($iMatched)) {
                    if (isset($aMatches[0][0]) === false) {
                        throw new \Exception('Regex matched but could not determine UUID from url: ' . $sUrl);
                    }

                    $aFoundParams[] = $aMatches[0][0];
                }
            }

            // Then replace the params and check if the route exists
            foreach ($aFoundParams as $i => $sParam) {
                // NOTE: re-write this for future-proofing
                // We already know the parameters from $this->aEndpoints
                // There is a smarter way to do this; for now this works.

                // In SnelStart api: the first parameter is almost always the {id}
                if ($i === 0) {
                    $sNewUrl = str_replace($sParam, '{id}', $sUrl);
                }

                // In SnelStart api: the second parameter is almost always the {bid}
                if ($i === 1) {
                    $sNewUrl = str_replace($sParam, '{bid}', $sUrl);
                }
            }

            // No new url was formulated from parameters, use the original url
            if (isset($sNewUrl) === false) {
                $sNewUrl = $sUrl;
            }

            // NOTE: should this done here or at the definition of the routes in $this->aEndpoints?
            if (strpos($sRoute, '/') === 0) {
                $sRoute = substr($sRoute, 1);
            }

            if ($sRoute === $sNewUrl) {
                if (isset($oRoute[$sType]) === false || in_array($sType, $aAllowedMethods) === false) {
                    throw new \Exception('The route was matched but the method ' . $sType . ' did not match, allowed methods are: ' . $sAllowedMethods);
                }

                // Endpoint matched
                return $oRoute[$sType];
            }
        }

        // No match
        return false;
    }

    // Retrieves a route parameter by name from a oRouteMetadata
    private function getRouteParameter($sName, $oRouteMetadata)
    {
        foreach ($oRouteMetadata['definitions'] as $sDefinitionName => $oDefinition) {
            if ($sDefinitionName === $sName) {
                return $oDefinition;
            }
        }

        return false;
    }

    // Validates a data array (guzzle) with a endpoint api route metadata
    private function validateRouteMetadata($aData, $oRouteMetadata)
    {
        // Empty data is always valid
        if (sizeof($aData) === 0) {
            return true;
        }

        $aMatchedDefinitions = [];
        $aDefinitions        = [];

        foreach ($oRouteMetadata['definitions'] as $sDefinitionName => $oDefinition) {
            if (in_array($sDefinitionName, $aDefinitions) === false) {
                $aDefinitions[] = $sDefinitionName;
            }
        }

        // Makes sure a form param exists
        if (array_key_exists('form_params', $aData)) {
            foreach ($aData['form_params'] as $sName => $mValue) {
                $oDefinitionMatch = null;

                foreach ($oRouteMetadata['definitions'] as $sDefinitionName => $oDefinition) {
                    if ($sDefinitionName === $sName) {
                        $oDefinitionMatch = $oDefinition;

                        $aMatchedDefinitions[] = $sDefinitionName;
                    }
                }
                if ($oDefinitionMatch === null) {
                    throw new \Exception('Invalid form parameter: ' . $sName);
                }
            }
        }

        // Makes sure we are not missing any param definitions
        // TODO: optional params?
        $aMissingDefinitions = array_diff($aDefinitions, $aMatchedDefinitions);

        foreach ($aMissingDefinitions as $key => $value) {
            $oRouteParameter = $this->getRouteParameter($value, $oRouteMetadata);

            $mDisplay = $oRouteParameter;

            // Display the items as the type it self if the type is a array
            if ($mDisplay === 'array') {
                $mDisplay = $oRouteParameter['items'];
            }

            $aMissingDefinitions[$value] = $mDisplay;
            unset($aMissingDefinitions[$key]);
        }

        if (sizeof($aMissingDefinitions) >= 1) {
            throw new \Exception('Missing form parameter: ' . var_export($aMissingDefinitions, true));
        }
    }

    // Verifies a route by matching swagger output with developer input
    private function verifyRoute($sType, $sUrl, $aData, $sBase)
    {
        // Only base api is verified
        if ($sBase !== 'base') {
            return true;
        }

        // Match the route with a valid endpoint
        return $this->matchEndpoint($sType, $sUrl);

    }

    // Formulates a request to send to a api
    public function request($sType, $sUrl, $aData = [], $sBase = 'base')
    {
        /** If not being called from the this package, make sure the $aData lives in form_params
        * So api calls can be made like this:
        * $this->oSnelStartManager->request('POST', 'verkoopboekingen', $aData);
        *
        * Instead of this:
        * $this->oSnelStartManager->request('POST', 'verkoopboekingen', [
        *     'form_params' => $aData,
        * ]);
        *
        * There is no need to supply different args (externally) except form_params
        **/
        $aBackTrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 1);
        if (strpos($aBackTrace[0]['file'], 'SnelstartApiBundle') === false) {
            $aData = [
                'form_params' => $aData,
            ];
        }

        $sType = strtoupper($sType);

        if (!in_array($sType, $this->aSettings['allowedtypes'])) {
            throw new \Exception($sType . ' is not a allowed type');
        }

        if (!array_key_exists($sBase, $this->aSettings['api'])) {
            throw new \Exception($sBase . ' api does not exist');
        }

        // Standard guzzle data that will be merged with user specified guzzle options
        $aDataStandard = [
            'connect_timeout' => $this->aSettings['timeouts']['connection'],
            'timeout'         => $this->aSettings['timeouts']['request'],
        ];

        // We want to pass some needed headers if this is not the auth api
        if ($sBase !== 'auth') {
            // Somehow the token was not generated yet
            if (property_exists($this, 'sAccessToken') === false) {
                throw new \Exception('Access token was not defined yet by ' . __CLASS__);
            }

            $aDataStandard['headers']['Content-Type']              = 'application/x-www-form-urlencoded';
            $aDataStandard['headers']['Ocp-Apim-Subscription-Key'] = $this->aSettings['creds']['primary'];
            $aDataStandard['headers']['Authorization']             = 'Bearer ' . $this->sAccessToken;
        }

        $aDataMerged = array_replace_recursive($aData, $aDataStandard);

        return $this->send($sType, $sUrl, $aDataMerged, $sBase);
    }

    // Sends the actual request to a api
    private function send($sType, $sUrl, $aData, $sBase)
    {
        // Lets not send a invalid url, data, route or method shall we
        $oRouteMetadata = $this->verifyRoute($sType, $sUrl, $aData, $sBase);

        if ($oRouteMetadata === false) {
            $this->routeMatchFailed($sUrl);
        }

        if (strpos($sUrl, '/') === 0) {
            $sUrl = str_replace('/', '', $sUrl);
        }

        // Make the full URL
        $sBaseUrl = $this->aSettings['api'][$sBase]['url'];
        $sUrl     = $sBaseUrl . $sUrl;

        try {
            $oRequest = $this->oGuzzle->request($sType, $sUrl, $aData);
        } catch (RequestException $oException) {
            // Unauthorized! (we assume the token has expired)
            if ($oException->getCode() === 401) {
                return $this->responseTokenExpired(__FUNCTION__, func_get_args());
            }

            // NOTE: Well, this could mean two things: empty response or an actual 404
            // We always expect a empty response.
            // NOTE: api should return appropriate response status code which is 204 (no content)
            if ($oException->getCode() === 404) {
                return $this->responseEmptyResult();
            }

            // Malformed request, why what how, lets find out
            if ($oException->getCode() === 400) {
                return $this->responseMalformedRequest($oException, $aData, $oRouteMetadata);
            }

            // Any other type of error? we raise an exception
            return $this->responseRaiseException($oException);
        }

        // Successful Response!
        return $this->response($oRequest);
    }

    // The route matching failed, try to guess the closest matching route to help the developer
    private function routeMatchFailed($sUrl)
    {
        $aRoutes = [];
        foreach ($this->aEndpoints as $sRoute => $oRoute) {
            $aRoutes[] = $sRoute;
        }

        $sClostestRoute = false;
        $iMinScore      = PHP_INT_MAX;

        foreach ($aRoutes as $i => $sCurStr) {
            $iScore = levenshtein($sUrl, $sCurStr);
            if ($iScore < $iMinScore) {
                $iMinScore      = $iScore;
                $sClostestRoute = $sCurStr;
            }
        }

        if ($sClostestRoute === false) {
            throw new \Exception('Could not find closest route');
        }

        if (strpos($sClostestRoute, '/') === 0) {
            $sClostestRoute = substr($sClostestRoute, 1);
        }

        throw new \Exception('Route ' . $sUrl . ' was not found. Did you mean: ' . $sClostestRoute . ' ?');
    }

    /**
    *    NOTE: This function can be called recursivly because generateAccessToken is called in it.
    *    Also the api could keep rejecting legitimate calls, we don't want endless loops
    *    Therefor; there is a retry counter implemented
    **/
    private function responseTokenExpired($sFunction, $aArguments)
    {
        $this->iRetries++;

        if ($this->iRetries >= 3) {
            throw new \Exception('Failed to retry request for 3 times in a row: [' . $aArguments[0] . '] ' . $aArguments[1]);
        }

        // Invalidate the CACHED token
        $this->oCache->deleteItem('snelstart.token');

        // Regenerate token
        $this->generateAccessToken();

        // Recall failed function (hax)
        call_user_func_array(__CLASS__ . '::' . $sFunction, $aArguments);
    }

    // Everything was validated except the form params and the api returned a 400 response.
    // Lets find out what happened
    private function responseMalformedRequest($oResponseError, $aData, $oRouteMetadata) {
        // Error message body, should include what went wrong
        $aErrors = $this->decodeResponse($oResponseError->getResponse());

        $oRequest        = $oResponseError->getRequest();
        $oUrl            = $oRequest->getUri();
        $sMethod         = $oRequest->getMethod();
        $sFullRequestUrl = $oUrl->getScheme() . '://' . $oUrl->getHost() . $oUrl->getPath();

        // Top level errors are from the API
        foreach ($aErrors as $oError) {
            throw new \Exception($oError->message);
        }

        // TODO: this validation code should be executed before the actual send request if validation
        // Is working 100% via swagger file
        try {
            // Validate the route data with the endpoint definitions
            $this->validateRouteMetadata($aData, $oRouteMetadata);
        } catch (\Exception $oException) {
            throw new \Exception($oResponseError->getMessage() . PHP_EOL . PHP_EOL . $oException->getMessage());
        }

        // The routeMetadata got validated but the server still responded with a 400 response.
        // This should not hit. but in theory could due to sloppy coding
        throw new \Exception('form parameters are validated but server still responded with a 400 response: ' . $oResponseError->getMessage());
    }

    // A response failed our expectations
    private function responseRaiseException($oException)
    {
        $sThrow = $oException->getMessage();

        if ($oException->getResponse() !== null) {
            $sThrow .= ' ' . $this->decodeResponse($oException->getResponse());
        }

        throw new \Exception($sThrow);
    }

    // A response should always be decodable from whatever format to a php object or array
    private function decodeResponse($oResponse)
    {
        $oReturn = $oResponse;

        // Sanity check
        if (method_exists($oResponse, 'getBody') === true) {
            $oReturn = $oReturn->getBody();
        }

        // We want to decode the api output if the content-type specified application/json
        if (strpos($oResponse->getHeaderLine('Content-type'), 'application/json') !== false) {
            $oReturn = json_decode($oReturn);

            if (json_last_error() !== JSON_ERROR_NONE) {
                throw new \Exception('Error parsing api output, invalid json: ' . var_export($oReturn, true));
            }
        }

        // Sanity check
        if (gettype($oReturn) !== 'object' && gettype($oReturn) !== 'array') {
            var_dump($oReturn);
            throw new \Exception('oReturn is not a object or array');
        }

        return $oReturn;
    }

    // Handle a response which is successful
    private function response($oResponse)
    {
        $oReturn = $this->decodeResponse($oResponse);

        // Reset the retry counter, useful for requests that do multiple api calls
        $this->iRetries = 0;

        return $oReturn;
    }

    // Best function ever
    // The response was empty from the api
    private function responseEmptyResult()
    {
        return null;
    }

    // Generate or get a cached access token from the snelstart api
    private function generateAccessToken()
    {
        $oCachedToken = $this->oCache->getItem('snelstart.token');

        if ($oCachedToken->isHit() === false) {
            $sDecoded  = explode(':', base64_decode($this->aSettings['creds']['application']));
            $oResponse = $this->request('POST', 'token', [
                'form_params' => [
                    'grant_type' => 'password',
                    'username'   => $sDecoded[0],
                    'password'   => $sDecoded[1],
                ],
            ], 'auth');

            if (property_exists($oResponse, 'access_token') === false) {
                throw new \Exception('No access_token property found');
            }

            if (property_exists($oResponse, 'expires_in') === false) {
                throw new \Exception('No expires_in property found');
            }

            if (property_exists($oResponse, 'token_type') === false) {
                throw new \Exception('No token_type property found');
            }

            if ($oResponse->token_type !== 'bearer') {
                throw new \Exception('Token type is not bearer');
            }

            $sToken = $oResponse->access_token;

            $oCachedToken->set($sToken);
            $oCachedToken->expiresAfter($oResponse->expires_in);
            $this->oCache->save($oCachedToken);
        }

        $this->sAccessToken = $oCachedToken->get();

        return $this->sAccessToken;
    }
}
